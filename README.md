# Paint Mixer
## Where paint drying becomes interesting again!

---

Highlights:

* Test Driven Development
* Dependency Injection and Usecase Patterns


## Build

Execute gradle build, output will be in *build/libs/paint-mixer-1.0.jar*
```
gradle build jar
```


## Usage
```bash
java -jar paint-mixer-1.0.jar /home/andrew/development/java/paint-mixer/src/test/resources/valid_input_random.txt
```

## Output
```bash
andrew@andrew-Z68XP-UD3 ~/development/java/paint-mixer/build/libs $ java -jar paint-mixer-1.0.jar /home/andrew/development/java/paint-mixer/src/test/resources/valid_input_complex.txt
2018-Apr-06 13:53:25 [main] INFO  com.confusedpenguins.paintmixer.domain.usecase.ParseInputFileUseCase  - Parsing Input File: /home/andrew/development/java/paint-mixer/src/test/resources/valid_input_complex.txt
2018-Apr-06 13:53:25 [main] INFO  com.confusedpenguins.paintmixer.domain.usecase.ProcessWorkLogUseCase  - Solution G M G M G
Solution: G M G M G
andrew@andrew-Z68XP-UD3 ~/development/java/paint-mixer/build/libs $
```
---