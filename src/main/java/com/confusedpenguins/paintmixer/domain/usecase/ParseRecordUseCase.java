package com.confusedpenguins.paintmixer.domain.usecase;

import com.confusedpenguins.paintmixer.domain.entity.ColorType;
import com.confusedpenguins.paintmixer.domain.entity.UnsupportedColorTypeException;
import com.confusedpenguins.paintmixer.domain.entity.customer.Customer;
import com.confusedpenguins.paintmixer.domain.entity.customer.CustomerRequest;
import com.confusedpenguins.paintmixer.domain.usecase.exception.InvalidRecordException;

import java.util.ArrayList;

/**
 * Takes a line from input file and returns the customer requests in an array list
 */
public class ParseRecordUseCase implements UseCase<Customer> {
    private final String inputLine;
    private final int maxColors;

    protected ParseRecordUseCase(int maxColors, String inputLine) {
        this.inputLine = inputLine;
        this.maxColors = maxColors;
    }

    @Override
    public Customer execute() throws InvalidRecordException, UnsupportedColorTypeException, NumberFormatException {
        if (inputLine == null) {
            throw new InvalidRecordException("Input line is null");
        }

        if (inputLine.isEmpty()) {
            throw new InvalidRecordException("Input line is empty");
        }

        // handle "1 G 2"

        String[] tokens = inputLine.split(" ");

        if (tokens.length % 2 != 0) {
            throw new InvalidRecordException(String.format("Input line contains odd number of tokens %d", tokens.length));
        }

        ArrayList<CustomerRequest> requests = new ArrayList<>();
        // navigate each tuple
        for (int iterationIndex = 0; iterationIndex < tokens.length; iterationIndex += 2) {
            int color = Integer.parseInt(tokens[iterationIndex]);
            ColorType type = ColorType.get(tokens[iterationIndex + 1]);

            if (color > maxColors || color <= 0) {
                throw new InvalidRecordException(String.format("%d is not a valid color", color));
            }

            requests.add(new CustomerRequest(color, type));
        }

        return new Customer(requests);
    }
}
