package com.confusedpenguins.paintmixer.domain.usecase;

public interface UseCase<E> {
    E execute() throws Exception;
}
