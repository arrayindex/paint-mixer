package com.confusedpenguins.paintmixer.domain.usecase;

import com.confusedpenguins.paintmixer.domain.entity.UnsupportedColorTypeException;
import com.confusedpenguins.paintmixer.domain.entity.customer.Customer;
import com.confusedpenguins.paintmixer.domain.entity.order.WorkLog;
import com.confusedpenguins.paintmixer.domain.usecase.exception.InvalidArgumentException;
import com.confusedpenguins.paintmixer.domain.usecase.exception.InvalidRecordException;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ParseInputFileUseCase implements UseCase<WorkLog> {
    private static final Logger logger = Logger.getLogger(ParseInputFileUseCase.class);
    private final UseCaseFactory factory;
    private final String inputFilePath;

    protected ParseInputFileUseCase(UseCaseFactory factory, String inputFilePath) {
        this.factory = factory;
        this.inputFilePath = inputFilePath;
    }

    @Override
    public WorkLog execute() throws InvalidRecordException, UnsupportedColorTypeException, InvalidArgumentException, IOException {
        if (inputFilePath == null) {
            throw new InvalidArgumentException("Input File Path is Null");
        }

        if (inputFilePath.isEmpty()) {
            throw new InvalidArgumentException("Input File Path is Blank");
        }

        try {
            logger.info(String.format("Parsing Input File: %s", inputFilePath));

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getInputFile()));

            // read first line of the input file
            String line = bufferedReader.readLine();
            int maxColors = factory.newParseHeaderUseCase(line).execute();

            List<Customer> customerList = new ArrayList();

            while ((line = bufferedReader.readLine()) != null) {
                customerList.add(factory.newParseRecordUseCase(line, maxColors).execute());
            }

            return new WorkLog(maxColors, customerList);
        } catch (FileNotFoundException fnfe) {
            throw new InvalidArgumentException(String.format("Input File Path (%s) is Missing", inputFilePath));
        }
    }

    private InputStream getInputFile() throws FileNotFoundException {
        return new FileInputStream(inputFilePath);
    }
}
