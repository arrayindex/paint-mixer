package com.confusedpenguins.paintmixer.domain.usecase;

import com.confusedpenguins.paintmixer.domain.entity.order.WorkLog;

public class UseCaseFactory {
    private final String delimiter = " ";

    public ParseRecordUseCase newParseRecordUseCase(String inputString, int maxColors) {
        return new ParseRecordUseCase(maxColors, inputString);
    }

    public ParseHeaderUseCase newParseHeaderUseCase(String inputString) {
        return new ParseHeaderUseCase(inputString);
    }

    public ParseInputFileUseCase newParseInputFileUseCase(String inputFilePath) {
        return new ParseInputFileUseCase(this, inputFilePath);
    }

    public ProcessWorkLogUseCase newProcessWorkLogUseCase(WorkLog workLog) {
        return new ProcessWorkLogUseCase(workLog, delimiter);
    }

    public ProcessArgumentsUseCase newProcessArgumentsUseCase(String[] args) {
        return new ProcessArgumentsUseCase(args);
    }
}

