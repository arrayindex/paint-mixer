package com.confusedpenguins.paintmixer.domain.usecase.exception;

public class InvalidRecordException extends Exception {
    public InvalidRecordException(String message) {
        super(message);
    }
}
