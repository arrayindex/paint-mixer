package com.confusedpenguins.paintmixer.domain.usecase;

import com.confusedpenguins.paintmixer.domain.usecase.exception.InvalidArgumentException;
import org.apache.log4j.Logger;

import java.io.File;

public class ProcessArgumentsUseCase implements UseCase<Boolean> {
    private static final Logger logger = Logger.getLogger(ProcessArgumentsUseCase.class);
    private final String[] args;

    protected ProcessArgumentsUseCase(String[] args) {
        this.args = args;
    }

    @Override
    public Boolean execute() throws InvalidArgumentException {
        if (args == null) {
            throw new InvalidArgumentException("Null Arguments passed");
        }

        if (args.length == 0) {
            throw new InvalidArgumentException("No Arguments Passed");
        }

        if (!fileExists(args[0])) {
            throw new InvalidArgumentException(String.format("Provided File is missing: %s", args[0]));
        }

        return true;
    }

    private Boolean fileExists(String filePathString) {
        File f = new File(filePathString);
        return (f.exists() && !f.isDirectory());
    }
}
