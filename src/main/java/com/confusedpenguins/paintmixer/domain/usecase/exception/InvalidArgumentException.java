package com.confusedpenguins.paintmixer.domain.usecase.exception;

public class InvalidArgumentException extends Exception {
    public InvalidArgumentException(String message) {
        super(message);
    }
}
