package com.confusedpenguins.paintmixer.domain.usecase;

import com.confusedpenguins.paintmixer.domain.usecase.exception.InvalidRecordException;

public class ParseHeaderUseCase implements UseCase<Integer> {
    private final String header;

    protected ParseHeaderUseCase(String header) {
        this.header = header;
    }

    @Override
    public Integer execute() throws InvalidRecordException {
        if (header == null) {
            throw new InvalidRecordException("Header is null");
        }

        if (header.isEmpty()) {
            throw new InvalidRecordException("Header is empty");
        }

        if (header.length() != 1) {
            throw new InvalidRecordException("Header contains too many characters");
        }

        try {
            return Integer.parseInt(header);
        } catch (NumberFormatException e) {
            throw new InvalidRecordException(String.format("%s is not a valid record", header));
        }
    }
}
