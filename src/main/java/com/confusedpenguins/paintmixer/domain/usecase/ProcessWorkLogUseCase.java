package com.confusedpenguins.paintmixer.domain.usecase;

import com.confusedpenguins.paintmixer.domain.entity.ColorType;
import com.confusedpenguins.paintmixer.domain.entity.customer.Customer;
import com.confusedpenguins.paintmixer.domain.entity.customer.CustomerRequest;
import com.confusedpenguins.paintmixer.domain.entity.order.WorkLog;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Notes to Self
 * <p>
 * Iterate over customer requests, if a customer has only one
 */
public class ProcessWorkLogUseCase implements UseCase<String> {
    private static final Logger logger = Logger.getLogger(ProcessWorkLogUseCase.class);

    private String[] solution;
    private final WorkLog workLog;
    private final String delimeter;

    protected ProcessWorkLogUseCase(WorkLog workLog, String outputDelimiter) {
        this.workLog = workLog;
        this.delimeter = outputDelimiter;
    }

    @Override
    public String execute() throws Exception {
        if (workLog == null) {
            logger.error("Null worklog passed");
            return "";
        }

        solution = new String[workLog.getMaxColors()];

        List<Customer> customers = workLog.getCustomerRequests();
        Collections.sort(customers);

        for (Customer customer : customers) {
            ArrayList<CustomerRequest> workRequests = customer.getRequests();

            if (isMandatory(workRequests)) {
                CustomerRequest cr = workRequests.get(0);

                Boolean success = setMandatoryColor(cr);

                if (!success) {
                    logger.warn(String.format("No Solution can be found for customer %s", customer.getId().toString()));
                    return "";
                }

                continue;
            }

            if (isCustomerSatisfied(customer)) {
                continue;
            }

            Collections.sort(customer.getRequests());

            setCandidateColor(customer.getRequests());
        }
        return formatOutput();
    }

    private String formatOutput() {
        for (int i = 0; i < solution.length; i++) {
            if (solution[i] == null) {
                solution[i] = ColorType.GLOSS.getColor();
            }
        }

        return String.join(delimeter, solution);
    }

    private Boolean isCustomerSatisfied(Customer customer) {
        for (CustomerRequest cr : customer.getRequests()) {
            if (isColorSet(cr) && colorMatches(cr)) {
                return true;
            }
        }

        return false;
    }

    private Boolean isMandatory(ArrayList<CustomerRequest> workRequests) {
        return workRequests.size() == 1;
    }

    private Boolean isColorSet(CustomerRequest customerRequest) {
        return solution[customerRequest.getColor() - 1] != null;
    }

    private Boolean colorMatches(CustomerRequest customerRequest) {
        return solution[customerRequest.getColor() - 1].equals(customerRequest.getType().getColor());
    }

    private Boolean setMandatoryColor(CustomerRequest customerRequest) {
        if (!isColorSet(customerRequest)) {
            solution[customerRequest.getColor() - 1] = customerRequest.getType().getColor();
            return true;
        }

        if (colorMatches(customerRequest)) {
            return true;
        }

        return false;
    }

    private Boolean setCandidateColor(ArrayList<CustomerRequest> workRequests) {
        for (CustomerRequest request : workRequests) {
            if (isColorSet(request) && colorMatches(request)) {
                // Customer already satisfied break out and move on
                return true;
            }

            if (!isColorSet(request)) {
                setMandatoryColor(request);
                return true;
            }
        }


        return false;
    }
}
