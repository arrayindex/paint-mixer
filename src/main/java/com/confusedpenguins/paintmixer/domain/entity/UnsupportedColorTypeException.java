package com.confusedpenguins.paintmixer.domain.entity;

public class UnsupportedColorTypeException extends Exception {
    public UnsupportedColorTypeException(String message) {
        super(message);
    }
}