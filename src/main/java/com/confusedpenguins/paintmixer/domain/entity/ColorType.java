package com.confusedpenguins.paintmixer.domain.entity;

public enum ColorType {
    GLOSS("G"),
    MATTE("M");

    private final String color;

    ColorType(String color) {
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }

    public static ColorType get(String color) throws UnsupportedColorTypeException {
        if (color == null) {
            throw new UnsupportedColorTypeException("Input ColorType is Null");
        }

        if (color.isEmpty()) {
            throw new UnsupportedColorTypeException("Empty Strings are not supported");
        }

        for (ColorType iteratorColorType : values()) {
            if (iteratorColorType.color.equals(color)) {
                return iteratorColorType;
            }
        }

        throw new UnsupportedColorTypeException(String.format("%s is not a supported color type", color));
    }
}
