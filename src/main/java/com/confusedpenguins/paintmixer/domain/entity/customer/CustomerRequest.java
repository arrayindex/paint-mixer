package com.confusedpenguins.paintmixer.domain.entity.customer;

import com.confusedpenguins.paintmixer.domain.entity.ColorType;

public class CustomerRequest implements Comparable<CustomerRequest> {
    private int color;
    private ColorType type;

    public CustomerRequest(int color, ColorType type) {
        this.color = color;
        this.type = type;
    }

    public int getColor() {
        return color;
    }

    public ColorType getType() {
        return type;
    }

    @Override
    public int compareTo(CustomerRequest o) {
        if (this.getType().equals(ColorType.GLOSS)) {
            return -1;
        }
        return 1;
    }

    @Override
    public String toString() {
        return "CustomerRequest{" +
                "color=" + color +
                ", type=" + type +
                '}';
    }
}
