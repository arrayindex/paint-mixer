package com.confusedpenguins.paintmixer.domain.entity.customer;

import java.util.ArrayList;
import java.util.UUID;

public class Customer implements Comparable<Customer> {
    private UUID id;
    private ArrayList<CustomerRequest> requests;

    public Customer(ArrayList<CustomerRequest> requests) {
        this.id = UUID.randomUUID();
        this.requests = requests;
    }

    public UUID getId() {
        return id;
    }

    public ArrayList<CustomerRequest> getRequests() {
        return requests;
    }

    @Override
    public int compareTo(Customer candidate) {
        return this.requests.size() - candidate.requests.size();
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", requests=" + requests +
                '}';
    }
}
