package com.confusedpenguins.paintmixer.domain.entity.order;

import com.confusedpenguins.paintmixer.domain.entity.customer.Customer;
import com.confusedpenguins.paintmixer.domain.entity.customer.CustomerRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WorkLog {
    private final int maxColors;
    private List<Customer> customerRequests;

    public WorkLog(int maxColors, List<Customer> customerRequests) {
        this.maxColors = maxColors;
        this.customerRequests = customerRequests;
    }

    public int getMaxColors() {
        return maxColors;
    }

    public List<Customer> getCustomerRequests() {
        return customerRequests;
    }

    @Override
    public String toString() {
        return "WorkLog{" +
                "maxColors=" + maxColors +
                ", customerRequests=" + customerRequests +
                '}';
    }
}
