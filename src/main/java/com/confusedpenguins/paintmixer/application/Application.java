package com.confusedpenguins.paintmixer.application;

import com.confusedpenguins.paintmixer.domain.entity.UnsupportedColorTypeException;
import com.confusedpenguins.paintmixer.domain.entity.order.WorkLog;
import com.confusedpenguins.paintmixer.domain.usecase.UseCaseFactory;
import com.confusedpenguins.paintmixer.domain.usecase.exception.InvalidArgumentException;
import com.confusedpenguins.paintmixer.domain.usecase.exception.InvalidRecordException;

import java.io.IOException;

public class Application {
    private UseCaseFactory factory;

    public Application(UseCaseFactory factory) {
        this.factory = factory;
    }

    public static void main(String[] args) {
        Application application = new Application(new UseCaseFactory());
        String output = application.run(args);

        System.out.println(String.format("Solution: %s", output));
    }

    public String run(String[] args) {
        try {
            boolean validArguments = factory.newProcessArgumentsUseCase(args).execute();

            if (!validArguments) {
                return "";
            }

            String filePath = args[0];

            WorkLog workLog = factory.newParseInputFileUseCase(filePath).execute();

            String output = factory.newProcessWorkLogUseCase(workLog).execute();

            if (output == null || output.isEmpty()) {
                System.err.println("No Solution possible");
                return "";
            }

            return output;

        } catch (InvalidArgumentException invalidArgumentException) {
            System.err.println(invalidArgumentException.getMessage());
        } catch (InvalidRecordException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } catch (UnsupportedColorTypeException e) {
            System.err.println(e.getMessage());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

        return "";
    }
}
