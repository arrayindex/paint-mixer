package com.confusedpenguins.paintmixer.application;

import com.confusedpenguins.paintmixer.domain.usecase.UseCaseFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ApplicationTest {
    private UseCaseFactory factory;
    private ClassLoader classLoader = getClass().getClassLoader();

    @Before
    public void setUp() throws Exception {
        factory = new UseCaseFactory();
    }

    @Test
    public void when_application_is_passed_valid_input_return_correct_result() throws Exception {
        String inputFile = classLoader.getResource("valid_input_simple.txt").getFile();
        String[] args = {inputFile};
        Application application = new Application(factory);
        Assert.assertEquals("Should return correct answer", "G G G G M", application.run(args));
    }

    @Test
    public void when_application_is_passed_null_args_should_not_fatal() throws Exception {
        Application application = new Application(factory);
        Assert.assertEquals("Should return correct answer", "", application.run(null));
    }

    @Test
    public void when_application_is_passed_bad_files_should_not_fatal() throws Exception {
        Application application = new Application(factory);

        String valid_input_no_solution = classLoader.getResource("valid_input_no_solution.txt").getFile();
        String[] args = {valid_input_no_solution};
        Assert.assertEquals("Should return correct answer", "", application.run(args));

        String valid_input_random_ext_no_solution = classLoader.getResource("valid_input_random_ext_no_solution.txt").getFile();
        String[] args_a = {valid_input_random_ext_no_solution};
        Assert.assertEquals("Should return correct answer", "", application.run(args_a));
    }

    @After
    public void tearDown() throws Exception {
        factory = null;
    }
}