package com.confusedpenguins.paintmixer.domain.entity;

import org.junit.Assert;
import org.junit.Test;

public class ColorTypeTest {

    @Test
    public void when_valid_color_types_are_given_should_return_appropriate_enum() throws UnsupportedColorTypeException {
        String matteType = "M";
        String glossType = "G";

        Assert.assertEquals("Should be Gloss Type", ColorType.GLOSS, ColorType.get(glossType));
        Assert.assertEquals("Should be Matte Type", ColorType.MATTE, ColorType.get(matteType));
    }

    @Test(expected = UnsupportedColorTypeException.class)
    public void when_null_is_passed_should_throw_exception() throws UnsupportedColorTypeException {
        String candidateColorType = null;
        ColorType.get(candidateColorType);
    }

    @Test(expected = UnsupportedColorTypeException.class)
    public void when_blank_is_passed_should_throw_exception() throws UnsupportedColorTypeException {
        String candidateColorType = "";
        ColorType.get(candidateColorType);
    }

    @Test(expected = UnsupportedColorTypeException.class)
    public void when_unsupported_string_is_passed_should_throw_exception() throws UnsupportedColorTypeException {
        String candidateColorType = "X";
        ColorType.get(candidateColorType);
    }
}