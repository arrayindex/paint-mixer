package com.confusedpenguins.paintmixer.domain.usecase;

import com.confusedpenguins.paintmixer.domain.usecase.exception.InvalidRecordException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ParseHeaderUseCaseTest {
    private UseCaseFactory factory;

    @Before
    public void setUp() throws Exception {
        factory = new UseCaseFactory();
    }

    @Test
    public void when_valid_record_passed_return_max_colors_allowed() throws Exception {
        String inputString = "9";
        int maxColors = factory.newParseHeaderUseCase(inputString).execute();

        Assert.assertEquals("Expected number of colors should be 9", 9, maxColors);
    }

    @Test(expected = InvalidRecordException.class)
    public void when_null_record_passed_throw_invalid_record_exception() throws Exception {
        String inputString = null;
        int maxColors = factory.newParseHeaderUseCase(inputString).execute();
    }

    @Test(expected = InvalidRecordException.class)
    public void when_blank_record_passed_throw_invalid_record_exception() throws Exception {
        String inputString = "";
        int maxColors = factory.newParseHeaderUseCase(inputString).execute();
    }

    @Test(expected = InvalidRecordException.class)
    public void when_invalid_record_passed_throw_invalid_record_exception() throws Exception {
        String inputString = "X";
        int maxColors = factory.newParseHeaderUseCase(inputString).execute();
    }

    @Test(expected = InvalidRecordException.class)
    public void when_invalid_record_with_too_many_characters_passed_throw_invalid_record_exception() throws Exception {
        String inputString = "1 2 3 4 5";
        int maxColors = factory.newParseHeaderUseCase(inputString).execute();
    }

    @After
    public void tearDown() throws Exception {
        factory = null;
    }
}