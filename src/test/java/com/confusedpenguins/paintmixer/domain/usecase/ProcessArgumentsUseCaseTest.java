package com.confusedpenguins.paintmixer.domain.usecase;

import com.confusedpenguins.paintmixer.domain.usecase.exception.InvalidArgumentException;
import com.confusedpenguins.paintmixer.domain.usecase.exception.InvalidRecordException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProcessArgumentsUseCaseTest {
    private UseCaseFactory factory;
    private ClassLoader classLoader = getClass().getClassLoader();

    @Before
    public void setUp() throws Exception {
        factory = new UseCaseFactory();
    }

    @Test(expected = InvalidArgumentException.class)
    public void when_null_args_are_passed_return_false() throws Exception {
        String[] args = null;
        factory.newProcessArgumentsUseCase(args).execute();
    }

    @Test(expected = InvalidArgumentException.class)
    public void when_no_args_are_passed_return_false() throws Exception {
        String[] args = {};
        factory.newProcessArgumentsUseCase(args).execute();
    }

    @Test(expected = InvalidArgumentException.class)
    public void when_first_arg_is_not_file_is_passed_return_false() throws Exception {
        String inputFile = "DEBUG";

        String[] args = {inputFile};
        factory.newProcessArgumentsUseCase(args).execute();
    }

    @Test
    public void when_valid_args_is_passed_return_true() throws Exception {
        String inputFile = classLoader.getResource("valid_input_simple.txt").getFile();
        String[] args = {inputFile};
        assertEquals("Valid arguments should return true", true, factory.newProcessArgumentsUseCase(args).execute());
    }


    @After
    public void tearDown() throws Exception {
        factory = null;
    }
}