package com.confusedpenguins.paintmixer.domain.usecase;

import com.confusedpenguins.paintmixer.domain.entity.UnsupportedColorTypeException;
import com.confusedpenguins.paintmixer.domain.entity.order.WorkLog;
import com.confusedpenguins.paintmixer.domain.usecase.exception.InvalidArgumentException;
import com.confusedpenguins.paintmixer.domain.usecase.exception.InvalidRecordException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ParseInputFileUseCaseTest {
    private UseCaseFactory factory;
    private ClassLoader classLoader = getClass().getClassLoader();

    @Before
    public void setUp() throws Exception {
        factory = new UseCaseFactory();
    }

    @Test
    public void when_valid_file_is_passed_should_return_work_orders() throws Exception {
        String inputFile = classLoader.getResource("valid_input_simple.txt").getFile();
        WorkLog workLog = factory.newParseInputFileUseCase(inputFile).execute();

        Assert.assertEquals("Expected number of colors should be 5", 5, workLog.getMaxColors());
        Assert.assertEquals("Expected number of orders should be 3", 3, workLog.getCustomerRequests().size());
    }

    @Test(expected = InvalidArgumentException.class)
    public void when_file_path_is_null_throw_invalid_argument_exception() throws Exception {
        String inputFile = null;
        factory.newParseInputFileUseCase(inputFile).execute();
    }

    @Test(expected = InvalidArgumentException.class)
    public void when_file_is_missing_throw_invalid_argument_exception() throws Exception {
        String inputFile = classLoader.getResource("valid_input_simple.txt").getFile();
        inputFile += "MISSINGFILE";
        factory.newParseInputFileUseCase(inputFile).execute();
    }

    @Test(expected = InvalidRecordException.class)
    public void when_invalid_file_is_missing_throw_invalid_record_exception() throws Exception {
        String inputFile = classLoader.getResource("invalid_missing_header_input_simple.txt").getFile();
        factory.newParseInputFileUseCase(inputFile).execute();
    }

    @Test(expected = UnsupportedColorTypeException.class)
    public void when_invalid_file_with_bad_color_type_is_missing_throw_invalid_record_exception() throws Exception {
        String inputFile = classLoader.getResource("invalid_bad_color_type_input_simple.txt").getFile();
        factory.newParseInputFileUseCase(inputFile).execute();
    }



    @Test(expected = InvalidRecordException.class)
    public void when_invalid_color_is_requested_should_not_fatal() throws Exception {
        String invalid_out_of_bounds_color_input_simple = classLoader.getResource("invalid_out_of_bounds_color_input_simple.txt").getFile();

        factory.newParseInputFileUseCase(invalid_out_of_bounds_color_input_simple).execute();
    }

    @After
    public void tearDown() throws Exception {
        factory = null;
    }
}