package com.confusedpenguins.paintmixer.domain.usecase;

import com.confusedpenguins.paintmixer.domain.entity.order.WorkLog;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class ProcessWorkLogUseCaseTest {
    private UseCaseFactory factory;
    private ClassLoader classLoader = getClass().getClassLoader();

    @Before
    public void setUp() throws Exception {
        factory = new UseCaseFactory();
    }

    @Test
    public void when_valid_work_logs_are_passed_return_correct_outputs() throws Exception {
        String valid_input_complex = classLoader.getResource("valid_input_complex.txt").getFile();
        String valid_input_simple = classLoader.getResource("valid_input_simple.txt").getFile();
        String valid_input_simple_edge_case_reprocess = classLoader.getResource("valid_input_simple_edge_case_reprocess.txt").getFile();
        String valid_input_random = classLoader.getResource("valid_input_random.txt").getFile();
        String valid_input_random_ext = classLoader.getResource("valid_input_random_ext.txt").getFile();

        WorkLog valid_input_simple_workLog = factory.newParseInputFileUseCase(valid_input_simple).execute();
        WorkLog valid_input_complex_workLog = factory.newParseInputFileUseCase(valid_input_complex).execute();
        WorkLog valid_input_simple_edge_case_reprocess_workLog = factory.newParseInputFileUseCase(valid_input_simple_edge_case_reprocess).execute();
        WorkLog valid_input_random_workLog = factory.newParseInputFileUseCase(valid_input_random).execute();
        WorkLog valid_input_random_ext_workLog = factory.newParseInputFileUseCase(valid_input_random_ext).execute();


        Assert.assertEquals("valid_input_complex_workLog should return correct solution", "G M G M G", factory.newProcessWorkLogUseCase(valid_input_complex_workLog).execute());
        Assert.assertEquals("valid_input_simple should return correct solution", "G G G G M", factory.newProcessWorkLogUseCase(valid_input_simple_workLog).execute());
        Assert.assertEquals("valid_input_simple_edge_case_reprocess_workLog should return correct solution", "M M", factory.newProcessWorkLogUseCase(valid_input_simple_edge_case_reprocess_workLog).execute());
        Assert.assertEquals("valid_input_random_workLog should return correct solution", "G G G G G G M", factory.newProcessWorkLogUseCase(valid_input_random_workLog).execute());
        Assert.assertEquals("valid_input_random_ext_workLog should return correct solution", "G G M G G G G", factory.newProcessWorkLogUseCase(valid_input_random_ext_workLog).execute());
    }

    @Test
    public void when_more_edge_cases_are_passed_return_correct_output() throws Exception {
        String valid_input_simple = classLoader.getResource("valid_input_edge_case.txt").getFile();
        WorkLog valid_input_simple_workLog = factory.newParseInputFileUseCase(valid_input_simple).execute();
        Assert.assertEquals("valid_input_simple should return correct solution", "M M", factory.newProcessWorkLogUseCase(valid_input_simple_workLog).execute());
    }

    @Test
    public void when_more_than_two_edge_cases_are_passed_return_correct_output() throws Exception {
        String valid_input_simple = classLoader.getResource("valid_input_edge_case_ext.txt").getFile();
        WorkLog valid_input_simple_workLog = factory.newParseInputFileUseCase(valid_input_simple).execute();
        Assert.assertEquals("valid_input_simple should return correct solution", "M M M", factory.newProcessWorkLogUseCase(valid_input_simple_workLog).execute());
    }

    @Test
    public void when_valid_work_logs_are_passed_with_no_solution_return_empty_string() throws Exception {
        String valid_input_no_solution = classLoader.getResource("valid_input_no_solution.txt").getFile();
        String valid_input_random_ext_no_solution = classLoader.getResource("valid_input_random_ext_no_solution.txt").getFile();

        WorkLog valid_input_no_solution_workLog = factory.newParseInputFileUseCase(valid_input_no_solution).execute();
        Assert.assertEquals("valid_input_no_solution_workLog should return correct solution", "", factory.newProcessWorkLogUseCase(valid_input_no_solution_workLog).execute());

        WorkLog valid_input_random_ext_no_solution_workLog = factory.newParseInputFileUseCase(valid_input_random_ext_no_solution).execute();
        Assert.assertEquals("valid_input_random_ext_no_solution_workLog should return correct solution", "", factory.newProcessWorkLogUseCase(valid_input_random_ext_no_solution_workLog).execute());


    }

    @Test
    public void when_null_work_logs_are_passed_with_no_solution_return_empty_string() throws Exception {
        WorkLog valid_input_no_solution_workLog = null;
        Assert.assertEquals("valid_input_no_solution_workLog should not fatal", "", factory.newProcessWorkLogUseCase(valid_input_no_solution_workLog).execute());
    }

    @After
    public void tearDown() throws Exception {
        factory = null;
    }
}