package com.confusedpenguins.paintmixer.domain.usecase;

import com.confusedpenguins.paintmixer.domain.entity.UnsupportedColorTypeException;
import com.confusedpenguins.paintmixer.domain.entity.customer.Customer;
import com.confusedpenguins.paintmixer.domain.entity.customer.CustomerRequest;
import com.confusedpenguins.paintmixer.domain.usecase.exception.InvalidRecordException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class ParseRecordUseCaseTest {
    UseCaseFactory factory;
    private final int maxColors = 5;

    @Before
    public void setUp() throws Exception {
        factory = new UseCaseFactory();
    }

    @Test
    public void when_valid_string_is_passed_should_return_list_of_requests() throws Exception {
        String inputString = "1 M 3 G 5 G";

        Customer output = this.factory.newParseRecordUseCase(inputString, maxColors).execute();
        Assert.assertEquals("Output should return three tuples", 3, output.getRequests().size());
    }

    @Test(expected = InvalidRecordException.class)
    public void when_invalid_string_with_odd_tokens_is_passed_should_throw_exception() throws Exception {
        String inputString = "1 M 3 G 5";
        this.factory.newParseRecordUseCase(inputString, maxColors).execute();
    }

    @Test(expected = InvalidRecordException.class)
    public void when_empty_string_is_passed_should_throw_exception() throws Exception {
        String inputString = "";
        this.factory.newParseRecordUseCase(inputString, maxColors).execute();
    }

    @Test(expected = UnsupportedColorTypeException.class)
    public void when_invalid_color_types_are_passed_should_throw_exception() throws Exception {
        String inputString = "5 5";
        this.factory.newParseRecordUseCase(inputString, maxColors).execute();
    }

    @Test(expected = InvalidRecordException.class)
    public void when_invalid_colors_are_passed_should_throw_exception() throws Exception {
        String inputString = "7 M";
        this.factory.newParseRecordUseCase(inputString, maxColors).execute();
    }

    @After
    public void tearDown() throws Exception {
        factory = null;
    }
}